#
# Cookbook:: joeb_logstash
# Spec:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

require 'spec_helper'

describe 'joeb_logstash::default' do
  describe package('logstash') do
       it { should be_installed }
  end
  describe package('elasticsearch') do
      it { should be_installed }
  end
  describe service('logstash') do
    it { should be_running }
    it { should be_enabled }
   end
    describe service('elasticsearch') do
      it { should be_running }
      it { should be_enabled }
   end
end