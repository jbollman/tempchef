#
# Cookbook:: joeb_logstash
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.


# ADD JAVA REPO
apt_repository 'JAVA' do
 uri 'ppa:webupd8team/java'
 distribution 'xenial'
 components ['main']
 key       'EEA14886'
end

#Add ES repo 
apt_repository 'elasticsearch' do
  uri 'https://artifacts.elastic.co/packages/5.x/apt'
  distribution ''
  components ['stable', 'main']
  key         'https://artifacts.elastic.co/GPG-KEY-elasticsearch'
  cache_rebuild false
end

# Add logstash REPO
apt_repository 'logstash' do
 uri 'https://artifacts.elastic.co/packages/5.x/apt'
 components ['stable', 'main']
 distribution ''
end

#update ALL repos
#execute "apt-get update" do
#  command "apt-get update"
#end

#install JAVA
 bash 'accept java license' do
  code <<-EOH
  locale-gen en_US.UTF-8
  update-locale LANG=en_US.UTF-8
  echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections
  echo debconf shared/accepted-oracle-license-v1-1 seen true | debconf-set-selections
  export SKIP_IPLIKE_INSTALL=1
  apt-get -q -y install oracle-java8-installer
  EOH
end

#TODO redo the bash above to utilize package 'oracle-java8-installer' 

# Install logstash,elasticsearch
package ['logstash', 'elasticsearch']

#Configure elastic
cookbook_file '/etc/elasticsearch/elasticsearch.yml' do
    source 'elasticsearch.yml'
    action :create
end

# Configure Logstash
cookbook_file '/etc/logstash/conf.d/logstash.conf' do
    source 'logstash.conf'
    action :create
end 

#start logstash
service 'logstash' do
    action [ :enable, :start ]
end

#start elastic
service 'elasticsearch' do
    action [ :enable, :start ]
end
